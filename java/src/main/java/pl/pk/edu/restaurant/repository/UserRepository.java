package pl.pk.edu.restaurant.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pk.edu.restaurant.beans.ApplicationUser;

public interface UserRepository extends CrudRepository<ApplicationUser, Integer> {
    ApplicationUser findByUsername(String username);
}
