package pl.pk.edu.restaurant.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pk.edu.restaurant.beans.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {

}
