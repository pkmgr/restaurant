package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.Order;
import pl.pk.edu.restaurant.services.OrderService;

@Controller
@RequestMapping(path="/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping()
    public @ResponseBody
    Iterable<Order> getAll() {
        return orderService.getAll();
    }

//    @GetMapping("/{id}")
//    public @ResponseBody Table getById(@PathVariable Integer id) {
//        return tableService.getById(id);
//    }
//
//    @DeleteMapping("/{id}")
//    public @ResponseBody void deleteById(@PathVariable Integer id){
//        tableService.remove(id);
//    }

    @PostMapping()
    public @ResponseBody Order add(@RequestBody Order value){
        return orderService.add(value);
    }

//    @PutMapping("/{id}")
//    public @ResponseBody Table update(@RequestBody Table table, @PathVariable Integer id){
//        table.setId(id);
//        return tableService.update(table);
//    }
}
