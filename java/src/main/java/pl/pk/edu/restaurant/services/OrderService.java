package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.Order;
import pl.pk.edu.restaurant.repository.OrderRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getAll() {
        List<Order> orders = new ArrayList<>();
        orderRepository.findAll().forEach(orders::add);
        return orders;
    }

    public Order getById(Integer id) { return orderRepository.findById(id).orElse(null); }

    public Order add(Order value){
        return orderRepository.save(value);
    }

    public void remove(Order table) { orderRepository.delete(table); }

    public void remove(Integer id) { orderRepository.deleteById(id); }

    public Order update(Order table) { return orderRepository.save(table); }
}
