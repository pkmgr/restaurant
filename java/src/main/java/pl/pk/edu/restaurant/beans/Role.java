package pl.pk.edu.restaurant.beans;

public enum Role {
    Admin,
    Manager,
    Client,
    Waiter
}
