package pl.pk.edu.restaurant.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class DishCategory {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    private String name;

    @OneToMany(targetEntity = Dish.class, mappedBy = "category")
    @JsonIgnoreProperties("category")
    private List<Dish> dishes = new ArrayList<>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setDishes(List<Dish> value){
        this.dishes = value;
    }

    public List<Dish> getDishes(){
        return this.dishes;
    }
}
