package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.DishCategory;
import pl.pk.edu.restaurant.services.DishCategoryService;

@Controller
@RequestMapping(path="/dishCategory")
public class DishCategoryController {
    @Autowired
    private DishCategoryService dishCategoryService;

    @GetMapping()
    public @ResponseBody
    Iterable<DishCategory> getAll() {
        return dishCategoryService.getAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody DishCategory getById(@PathVariable Integer id) {
        return dishCategoryService.getById(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody void deleteById(@PathVariable Integer id){
        dishCategoryService.remove(id);
    }

    @PostMapping()
    public @ResponseBody DishCategory add(@RequestBody DishCategory dish){
        return dishCategoryService.add(dish);
    }

    @PutMapping("/{id}")
    public @ResponseBody DishCategory update(@RequestBody DishCategory dish, @PathVariable Integer id){
        dish.setId(id);
        return dishCategoryService.update(dish);
    }
}
