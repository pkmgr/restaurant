package pl.pk.edu.restaurant.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pk.edu.restaurant.beans.Table;
import pl.pk.edu.restaurant.repository.TableRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TableService {
    @Autowired
    private TableRepository tableRepository;

    public List<Table> getAll() {
        List<Table> tables = new ArrayList<>();
        tableRepository.findAll().forEach(tables::add);
        return tables;
    }

    public Table getById(Integer id) { return tableRepository.findById(id).orElse(null); }

    public Table add(Table table){
        return tableRepository.save(table);
    }

    public void remove(Table table) { tableRepository.delete(table); }

    public void remove(Integer id) { tableRepository.deleteById(id); }

    public Table update(Table table) { return tableRepository.save(table); }
}
