package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.Dish;
import pl.pk.edu.restaurant.beans.Table;
import pl.pk.edu.restaurant.services.DishService;
import pl.pk.edu.restaurant.services.TableService;

@Controller
@RequestMapping(path="/dish")
public class DishController {
    @Autowired
    private DishService dishService;

    @GetMapping()
    public @ResponseBody
    Iterable<Dish> getAll() {
        return dishService.getAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Dish getById(@PathVariable Integer id) {
        return dishService.getById(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody void deleteById(@PathVariable Integer id){
        dishService.remove(id);
    }

    @PostMapping()
    public @ResponseBody Dish add(@RequestBody Dish dish){
        return dishService.add(dish);
    }

    @PutMapping("/{id}")
    public @ResponseBody Dish update(@RequestBody Dish dish, @PathVariable Integer id){
        dish.setId(id);
        return dishService.update(dish);
    }
}
