package pl.pk.edu.restaurant.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.FORBIDDEN, reason = "Token is not valid")
public class ExpiredJWTException extends RuntimeException {
}