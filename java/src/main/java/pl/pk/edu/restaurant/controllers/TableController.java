package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.Table;
import pl.pk.edu.restaurant.services.TableService;

@Controller
@RequestMapping(path="/table")
public class TableController {
    @Autowired
    private TableService tableService;

    @GetMapping()
    public @ResponseBody
    Iterable<Table> getAll() {
        return tableService.getAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody Table getById(@PathVariable Integer id) {
        return tableService.getById(id);
    }

    @DeleteMapping("/{id}")
    public @ResponseBody void deleteById(@PathVariable Integer id){
        tableService.remove(id);
    }

    @PostMapping()
    public @ResponseBody Table add(@RequestBody Table table){
        return tableService.add(table);
    }

    @PutMapping("/{id}")
    public @ResponseBody Table update(@RequestBody Table table, @PathVariable Integer id){
        table.setId(id);
        return tableService.update(table);
    }
}
