package pl.pk.edu.restaurant.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.pk.edu.restaurant.beans.ApplicationUser;
import pl.pk.edu.restaurant.services.UserService;

@Controller
@RequestMapping(path="/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping()
    public @ResponseBody Iterable<ApplicationUser> getAllUsers() {
        return userService.getAll();
    }

    @GetMapping("/{id}")
    public @ResponseBody ApplicationUser getUserById(@PathVariable Integer id) {
        return userService.getById(id);
    }

    @GetMapping("/{username}")
    public @ResponseBody ApplicationUser getUserById(@PathVariable String username) {
        return userService.getByUsername(username);
    }

    @PostMapping()
    public @ResponseBody ApplicationUser addNewUser (@RequestBody ApplicationUser user) {
        return userService.add(user);
    }

    @PutMapping("/{id}")
    public @ResponseBody ApplicationUser updateUser (@RequestBody ApplicationUser user, @PathVariable Integer id) {
        user.setId(id);
        return userService.add(user);
    }
}
