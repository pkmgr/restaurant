package pl.pk.edu.restaurant.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Dish {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private String name;
    private String shortDescription;
    private String fullDescription;
    private String img;
    private Float price;
    @ManyToOne
    @JsonIgnoreProperties("dishes")
    private DishCategory category;

    @ManyToMany
    private List<Order> orders = new ArrayList<>();

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setShortDescription(String description){
        this.shortDescription = description;
    }

    public String getShortDescription(){
        return this.shortDescription;
    }

    public void setFullDescription(String description){
        this.fullDescription = description;
    }

    public String getFullDescription(){
        return this.fullDescription;
    }

    public void setPrice(Float value){
        this.price = value;
    }

    public Float getPrice(){
        return this.price;
    }

    public void setImg(String value){
        this.img = value;
    }

    public String getImg(){
        return this.img;
    }

    public void setCategory(DishCategory value){
        this.category = value;
    }

    public DishCategory getCategory(){
        return this.category;
    }
}
