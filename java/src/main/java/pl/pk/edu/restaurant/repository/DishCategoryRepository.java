package pl.pk.edu.restaurant.repository;

import org.springframework.data.repository.CrudRepository;
import pl.pk.edu.restaurant.beans.DishCategory;

public interface DishCategoryRepository extends CrudRepository<DishCategory, Integer> {

}
