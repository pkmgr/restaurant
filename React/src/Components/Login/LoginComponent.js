import React from "react";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import "./Login.css";

class LoginComponent extends React.Component {

    constructor() {
        super();
        this.state = {
            err: ''
        };
    }

    login (e) {
        e.preventDefault();
        var username = e.target.elements.username.value;
        var password = e.target.elements.password.value;
        if(username === 'abc' && password === '123') {
            localStorage.setItem('token', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE2MTAwMjM1MTYsImV4cCI6MTY0MTU1OTUxNiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.MvDWYDJ7eiKZqkEHXHKBq2ugScktt_JhVN7IRPvC_98');
            this.props.history.push('/adminpanel');
            console.log(localStorage);

        } else {
            this.setState({
                err: 'Invalid'
            });

        }
    }


render() {

    let format = {
        color:"red"
    };

    return (
        <div className="Login">           
            <form method="post" onSubmit={this.login.bind(this)}>
                
            <Form.Group size="lg" controlId="username">
                <Form.Label>Login</Form.Label>
                <Form.Control
                    autoFocus
                    type="username"
                />
            </Form.Group>
                    
            <Form.Group size="lg" controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control
                    type="password"
                />
            </Form.Group>

            <Button block size="lg" type="submit" value="Login" >
                Login
            </Button>

            <span style={format}> {this.state.err !== '' ? this.state.err: ''} </span>

            </form>
        </div>
        );

    }
}
export default LoginComponent;