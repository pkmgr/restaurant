import React from 'react'
import { Button } from 'react-bootstrap';
import { connect, useSelector } from 'react-redux';
import { removeBasket2 } from '../../actions/addActions';
import Table from 'react-bootstrap/Table'

function AdminPanel({basketProps, removeBasket2}) {
    
    console.log("basketPanelProps")
    console.log(basketProps);

    const baskets2 = useSelector(state => state.baskets),
        { data } = baskets2;

    if(!localStorage.getItem('token')) {
        return <div></div>;
    }
    
    return (
        <div className="container-products">
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Table</th>
                        <th>Products</th>
                        <th>QUANTITY</th>
                        <th>Price</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    { data.map((item, key) => {
                        const products = item.data;

                        return(
                            <tr key={ key }>
                                <td>{ item.table }</td>
                                <td>{ products.map((subitem, subkey) => {
                                   return (
                                       <div>{ subitem.name }</div>
                                   ) 
                                })}</td>

                                <td>{ products.map((subitem, subkey) => {
                                   return (
                                       <div>{ subitem.numbers }</div>
                                   ) 
                                })}</td>

                                <td>{ products.map((subitem, subkey) => {
                                   return (
                                       <div>{ subitem.numbers * subitem.price }</div>
                                   ) 
                                })}</td>
                                <td>
                                    <Button 
                                        onClick={ () => removeBasket2(key) }
                                        color='danger' 
                                        type='button'
                                    >Usuń</Button>
                                </td>

                                <td>
                                    <Button 
                                        onClick={ () => removeBasket2(key) }
                                        color='green' 
                                        type='button'
                                    >Zrealizowane</Button>
                                </td>
                            </tr>
                        )
                    }) }
                    
                </tbody>
            </Table>
        </div>

    )
}

const mapStateToProps = state => ({
    basketProps: state.basketState 
});

export default connect (mapStateToProps, { removeBasket2 })(AdminPanel);
