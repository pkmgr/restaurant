import React, { Fragment, useEffect, useState } from 'react'
import { connect, useSelector } from 'react-redux';

import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import { addBasket2 } from '../../actions/addActions';
import { removeBasket2 } from '../../actions/addActions';

import BagelAndCreamCheese from '../../Images/Breakfast/breakfast1.png'
import BreakfastSandwich from '../../Images/Breakfast/breakfast2.png'
import BakedChicken from '../../Images/Breakfast/breakfast3.png'
import EggsBenedict from '../../Images/Breakfast/breakfast4.png'
import ToastCroissantFriedegg from '../../Images/Breakfast/breakfast5.png'
import FullBreakfastFriedEggToastBrunch from '../../Images/Breakfast/breakfast6.png'
 

function Cart({basketProps, addBasket2, removeBasket2}) {
    console.log("basketProps")
    console.log(basketProps);

    const [number, setnumber] = useState(""),
        [ productsInCart, setProductsInCart ] = useState(null),
        [ send, setSend ] = useState(false),
        baskets2 = useSelector(state => state.baskets);

    function handleSubmit(event) {
        event.preventDefault();

        const basket = {
            table: number,
            data: productsInCart
        }

        addBasket2(basket);
    }

    useEffect(() => {
        if(baskets2.data.length > 0) {
            setSend(baskets2.data.length + 1);
            setTimeout(() => {
                setSend(false);
                setnumber("");
                
            }, 5000);
        }
    }, [ baskets2 ]);

    function validateForm() {
        return number.length > 0;
    }

    useEffect(() => {
        let temp = [];

        Object.keys(basketProps.products).forEach( function(item){
            console.log(item);
            console.log(basketProps.products[item].inCart);
            if(basketProps.products[item].inCart) {
                temp.push(basketProps.products[item])
            }
            console.log(productsInCart);
        });

        setProductsInCart(temp);
    }, [ basketProps ]);

    let productImages = [BagelAndCreamCheese, BreakfastSandwich, BakedChicken, EggsBenedict, ToastCroissantFriedegg, FullBreakfastFriedEggToastBrunch];

    const generateProductsInCart = () => {
        if(!productsInCart) {
            return null;
        }

        return productsInCart.map( (product, index ) => {
            console.log("My product is");
            console.log(product);
            return (
                <Fragment>
                    <div className="product"><ion-icon name="close-circle"></ion-icon><img alt="" src={productImages[index]} />
                        <span className="sm-hide">{product.name}</span>
                    </div>
                    <div className="price sm-hide">${product.price},00 </div>
                    <div className="quantity">
                        <ion-icon className="decrease" name="arrow-back-circle-outline"></ion-icon>
                        <span>{product.numbers}</span>
                        <ion-icon className="increase" name="arrow-forward-circle-outline"></ion-icon>
                    </div>
                    <div className="total">${product.numbers * product.price},00</div>
                </Fragment>

            )
        });
    };

 

    return (
        <div className="container-products">
            <div className="product-header">
                <h5 className="product-title">PRODUCT</h5>
                <h5 className="price sm-hide">PRICE</h5>
                <h5 className="quantity">QUANTITY</h5>
                <h5 className="total">TOTAL</h5>

            </div>
            <div className="products">
                { generateProductsInCart() }
            </div>
            <div className="basketTotalContainer">
                <h4 className="basketTotalTitle">Basket Total</h4>
                <h4 className="basketTotal">{basketProps.cartCost},00</h4>
            </div>


            <div className="Login">
             <Form onSubmit={handleSubmit}>
                    <Form.Group size="lg" controlId="number">
                    <Form.Label>Table number:</Form.Label>
                        <Form.Control
                            autoFocus
                            type="number"
                            value={number}
                            onChange={(e) => setnumber(e.target.value)}
                        />
                </Form.Group>
                    <Button type='submit' disabled={!validateForm() }  onClick={ () => removeBasket2(basketProps) }>
                    Order
                    </Button>
                </Form>
    
            </div>
        </div>

    )
}

const mapStateToProps = state => ({
    basketProps: state.basketState,
    baskets: state.baskets 
});

export default connect (mapStateToProps, { addBasket2, removeBasket2 })(Cart);
