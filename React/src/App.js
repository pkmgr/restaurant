import React from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Header from './Components/Header/Header';
import HomePage from './Components/HomePage/HomePage';
//import Login from './Components/Login/Login';
import Signup from './Components/Signup/Signup';
import ItemDetail from './Components/ItemDetail/ItemDetail';
import Cart from './Components/Cart/Cart';
import AdminPanel from './Components/AdminPanel/AdminPanel';
import { Provider } from 'react-redux';
import store from './store.js'

import LoginComponent from './Components/Login/LoginComponent';
import WelcomeComponent from './Components/Login/WelcomeComponent';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <BrowserRouter>
          <Header />
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/cart" component={Cart} />

            <Route exact path="/LoginComponent" component={LoginComponent}/> 
             
            <Route exact path="/welcome/:username" component={WelcomeComponent}></Route>
            <Route exact path="/logout" component={LoginComponent}></Route>

            <Route exact path="/signup">
              <Signup />
            </Route>

            <Route exact path="/adminpanel" component={AdminPanel} />

            <Route path="/fooditem/:foodId">
            <ItemDetail/>
          </Route>
          </Switch>     
          </BrowserRouter> 
      </div>
    </Provider>
  );
}

export default App;
